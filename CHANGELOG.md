# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.3.7] - 2023-11-13
### Changed
- 调整控制台脚本.
- 调整目录结构.

## [1.3.6] - 2023-11-09
### Changed
- 调整控制台脚本.
- 调整控制台脚本.

## [1.3.5] - 2023-11-09
### Changed
- 调整控制台脚本.

## [1.3.4] - 2023-11-09
### Changed
- 调整控制台脚本.

## [1.3.3] - 2023-11-09
### Changed
- 调整控制台脚本.

## [1.3.2] - 2023-11-09
### Changed
- 调整控制台脚本.

## [1.3.1] - 2023-11-09
### Changed
- 调整控制台脚本.

## [1.3.0] - 2023-11-09
### Changed
- 调整控制台脚本.
- 调整目录结构.
- 增加本地调试的包引入.
- 调整代码结构.
- 修订非交易时间的显示信息.
- 调整代码结构.
- 增加判断交易时间的函数.
- 增加判断交易时间的函数.
- 增加盘中扫描订单的时间.
- 去掉循环阻断, 一直循环.
- Trader增加查询资产和买入两个方法.
- 调整miniQMT交易代码.
- 测试xtdata.
- 调整日志记录器.
- 增加单例模式.
- 增加日志记录器.
- 新增xtquant交易对象.

## [1.2.5] - 2023-11-08
### Changed
- 统一转化换行符.
- 统一换行符.

## [1.2.4] - 2023-11-08
### Changed
- Add LICENSE.

## [1.2.3] - 2023-11-08
### Changed
- 修订清理发布的临时文件夹.

## [1.2.2] - 2023-11-08
### Changed
- 应用返回值, 方便windows计划任务观察运行结果.

## [1.2.1] - 2023-11-06
### Changed
- 删除错误的的提示信息.

## [1.2.0] - 2023-11-06
### Changed
- 修复tick数据全推的列表为全部持仓的个股列表.

## [1.1.10] - 2023-11-03
### Changed
- 涨停板不卖, 需要手动处理.

## [1.1.9] - 2023-10-30
### Changed
- 优化用户宿主目录的获取方法.

## [1.1.8] - 2023-10-24
### Changed
- 修订买入价格异常的问题, 原因是tick返回中卖4和卖5的价格为0.

## [1.1.7] - 2023-09-22
### Changed
- 优化imports.
- 明确源文件的编码格式.

## [1.1.6] - 2023-09-22
### Changed
- 调整交易协议.
- 增加交易协议.
- 增加交易协议.
- 修订protobuf协议.
- 增加protobuf协议.

## [1.1.5] - 2023-09-21
### Changed
- 调整依赖库兼容版本.

## [1.1.4] - 2023-09-21
### Changed
- 调整依赖库兼容版本.

## [1.1.3] - 2023-09-21
### Changed
- 调整依赖库兼容版本.

## [1.1.2] - 2023-09-21
### Changed
- 调整依赖库兼容版本.

## [1.1.1] - 2023-09-20
### Changed
- 修订Quant1X-Xtquant版本.

## [1.1.0] - 2023-09-20
### Changed
- Qmt 拆分出去xtquant独立维护.

## [1.0.22] - 2023-09-20
### Changed
- 增加早盘交易标识.
- 调整文件.

## [1.0.21] - 2023-09-18
### Changed
- 调整配置文件路径.

## [1.0.20] - 2023-09-10
### Changed
- 增加GitPython.
- Update changelog.

## [1.0.19] - 2023-09-10
### Changed
- .gitignore 增加csv文件.

## [1.0.18] - 2023-09-09
### Changed
- 修订函数注释.

## [1.0.17] - 2023-09-09
### Changed
- 修订README.md.
- 增加windws计划任务配置文件.
- 优化安装脚本.
- 优化安装脚本.

## [1.0.16] - 2023-09-09
### Changed
- 优化安装脚本.

## [1.0.15] - 2023-09-09
### Changed
- 优化安装脚本.

## [1.0.14] - 2023-09-09
### Changed
- Change setup.py.

## [1.0.13] - 2023-09-09
### Changed
- 自动检测QMT安装路径.

## [1.0.12] - 2023-09-09
### Changed
- 优化imports.
- 增加实验代码.
- 增加实验代码.
- 增加实验代码.
- 增加实验代码.

## [1.0.11] - 2023-09-09
### Changed
- 使用tag上的版本号.
- 测试获取git 最新tag.

## [1.0.10] - 2023-09-09
### Changed
- 补充依赖列表.

## [1.0.9] - 2023-09-09
### Changed
- 增加git过滤类型.
- 增加新数据文件.
- 增加发布脚本.
- 增加pip setup 配置文件.
- 增加实验代码.
- 重构交易主程序.

## [1.0.8] - 2023-09-06
### Changed
- 更新新版本的xtquant.
- 会话session id用秒数代替.
- 修订README.
- 修订miniQMT路径.
- 增加判断今日是否交易日.

## [1.0.7] - 2023-09-01
### Changed
- 添加遗漏的dll.

## [1.0.6] - 2023-09-01
### Changed
- 忽略pyhon的缓存文件目录.

## [1.0.5] - 2023-09-01
### Changed
- 增加miniQMT使用的配置文件.
- 增加README.

## [1.0.4] - 2023-08-31
### Changed
- 修订错误的单词.
- 增加仓库忽略的目录.

## [1.0.3] - 2023-08-24
### Changed
- 用tick最新的卖5买入.

## [1.0.2] - 2023-08-21
### Changed
- 增加单笔最大买入金额.

## [1.0.1] - 2023-08-15
### Changed
- 增加在休市时间撤销未成交的委托订单.

## [1.0.0] - 2023-08-14
### Changed
- 用开盘价+5分代替卖5的价格.
- 修复证券代码转换的bug.
- 增加测试代码.
- 优化交易逻辑.
- 新增卖出时间段.
- 完善miniQMT.
- 优化代码.
- 增加QMT交易代码.
- 调整函数名为小写加下划线的格式.
- 去掉无用的控制台输出.
- 增加批量下载数据接口的引用.
- QMT源文件改名.
- 增加依赖文本.
- 优化交易主流程.
- 构建QMT上下文类.
- 优化代码.
- 优化代码.
- 格式化xttype代码.
- 格式化xtconstant代码.
- Git仓库去掉临时文件.
- 增加.gitignore.
- First commit.

[Unreleased]: https://gitee.com/quant1x/qmt/compare/v1.3.7...HEAD
[1.3.7]: https://gitee.com/quant1x/qmt/compare/v1.3.6...v1.3.7
[1.3.6]: https://gitee.com/quant1x/qmt/compare/v1.3.5...v1.3.6
[1.3.5]: https://gitee.com/quant1x/qmt/compare/v1.3.4...v1.3.5
[1.3.4]: https://gitee.com/quant1x/qmt/compare/v1.3.3...v1.3.4
[1.3.3]: https://gitee.com/quant1x/qmt/compare/v1.3.2...v1.3.3
[1.3.2]: https://gitee.com/quant1x/qmt/compare/v1.3.1...v1.3.2
[1.3.1]: https://gitee.com/quant1x/qmt/compare/v1.3.0...v1.3.1
[1.3.0]: https://gitee.com/quant1x/qmt/compare/v1.2.5...v1.3.0
[1.2.5]: https://gitee.com/quant1x/qmt/compare/v1.2.4...v1.2.5
[1.2.4]: https://gitee.com/quant1x/qmt/compare/v1.2.3...v1.2.4
[1.2.3]: https://gitee.com/quant1x/qmt/compare/v1.2.2...v1.2.3
[1.2.2]: https://gitee.com/quant1x/qmt/compare/v1.2.1...v1.2.2
[1.2.1]: https://gitee.com/quant1x/qmt/compare/v1.2.0...v1.2.1
[1.2.0]: https://gitee.com/quant1x/qmt/compare/v1.1.10...v1.2.0
[1.1.10]: https://gitee.com/quant1x/qmt/compare/v1.1.9...v1.1.10
[1.1.9]: https://gitee.com/quant1x/qmt/compare/v1.1.8...v1.1.9
[1.1.8]: https://gitee.com/quant1x/qmt/compare/v1.1.7...v1.1.8
[1.1.7]: https://gitee.com/quant1x/qmt/compare/v1.1.6...v1.1.7
[1.1.6]: https://gitee.com/quant1x/qmt/compare/v1.1.5...v1.1.6
[1.1.5]: https://gitee.com/quant1x/qmt/compare/v1.1.4...v1.1.5
[1.1.4]: https://gitee.com/quant1x/qmt/compare/v1.1.3...v1.1.4
[1.1.3]: https://gitee.com/quant1x/qmt/compare/v1.1.2...v1.1.3
[1.1.2]: https://gitee.com/quant1x/qmt/compare/v1.1.1...v1.1.2
[1.1.1]: https://gitee.com/quant1x/qmt/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitee.com/quant1x/qmt/compare/v1.0.22...v1.1.0
[1.0.22]: https://gitee.com/quant1x/qmt/compare/v1.0.21...v1.0.22
[1.0.21]: https://gitee.com/quant1x/qmt/compare/v1.0.20...v1.0.21
[1.0.20]: https://gitee.com/quant1x/qmt/compare/v1.0.19...v1.0.20
[1.0.19]: https://gitee.com/quant1x/qmt/compare/v1.0.18...v1.0.19
[1.0.18]: https://gitee.com/quant1x/qmt/compare/v1.0.17...v1.0.18
[1.0.17]: https://gitee.com/quant1x/qmt/compare/v1.0.16...v1.0.17
[1.0.16]: https://gitee.com/quant1x/qmt/compare/v1.0.15...v1.0.16
[1.0.15]: https://gitee.com/quant1x/qmt/compare/v1.0.14...v1.0.15
[1.0.14]: https://gitee.com/quant1x/qmt/compare/v1.0.13...v1.0.14
[1.0.13]: https://gitee.com/quant1x/qmt/compare/v1.0.12...v1.0.13
[1.0.12]: https://gitee.com/quant1x/qmt/compare/v1.0.11...v1.0.12
[1.0.11]: https://gitee.com/quant1x/qmt/compare/v1.0.10...v1.0.11
[1.0.10]: https://gitee.com/quant1x/qmt/compare/v1.0.9...v1.0.10
[1.0.9]: https://gitee.com/quant1x/qmt/compare/v1.0.8...v1.0.9
[1.0.8]: https://gitee.com/quant1x/qmt/compare/v1.0.7...v1.0.8
[1.0.7]: https://gitee.com/quant1x/qmt/compare/v1.0.6...v1.0.7
[1.0.6]: https://gitee.com/quant1x/qmt/compare/v1.0.5...v1.0.6
[1.0.5]: https://gitee.com/quant1x/qmt/compare/v1.0.4...v1.0.5
[1.0.4]: https://gitee.com/quant1x/qmt/compare/v1.0.3...v1.0.4
[1.0.3]: https://gitee.com/quant1x/qmt/compare/v1.0.2...v1.0.3
[1.0.2]: https://gitee.com/quant1x/qmt/compare/v1.0.1...v1.0.2
[1.0.1]: https://gitee.com/quant1x/qmt/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitee.com/quant1x/qmt/releases/tag/v1.0.0
