# coding=utf-8
import getpass
import winreg

import win32api
import win32con


def find_path(name="XtltClient.exe"):  # 查找的软件名称
    path = None
    key = rf'SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\{name}'
    # 通过获取Windows注册表查找软件
    key = win32api.RegOpenKey(win32con.HKEY_LOCAL_MACHINE, key, 0, win32con.KEY_READ)
    info2 = win32api.RegQueryInfoKey(key)
    for j in range(0, info2[1]):
        key_value = win32api.RegEnumValue(key, j)[1]
        if key_value.upper().endswith(name.upper()):
            path = key_value
            break
    win32api.RegCloseKey(key)
    return path  # 返回查找到的安装路径


def find_path2(name="XtltClient.exe"):  # 查找的软件名称
    path = None
    root = winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE)
    app_key = rf'SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\{name}'
    # 通过获取Windows注册表查找软件
    key = winreg.OpenKey(root, app_key)
    info2 = winreg.QueryInfoKey(key)
    for j in range(0, info2[1]):
        key_value = win32api.RegEnumValue(key, j)[1]
        if key_value.upper().endswith(name.upper()):
            path = key_value
            break
    winreg.CloseKey(key)
    return path  # 返回查找到的安装路径


# 下面可忽略
# sys.path.append(find_path())#将获取的安装路径添加到环境变量
# app = 'XtltClient.exe'
# result = find_path2(app)
# print(result)

username = getpass.getuser()
print('username:', username)
lnk = r'C:\Users\wangfeng\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\国金证券QMT交易端\启动国金证券QMT交易端.lnk'
import win32com.client

shell = win32com.client.Dispatch("WScript.Shell")
shortcut = shell.CreateShortCut(lnk)
print(shortcut.Targetpath)
qjzqpath = str(shortcut.Targetpath).split('bin.x64')
print(qjzqpath)
