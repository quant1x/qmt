# encoding:gbk
#
# 国金证券QMT, v1.2.9
#
# 1.2.9 调整止损比例为0, 立即卖出
# 1.2.8 调整止盈比例为0, 立即卖出
# 1.2.7 调整可用金额算法
# 1.2.6 调整扣去买入可能潜在的费用
# 1.2.5 修订日志文件编码格式为utf-8
# 1.2.4 调整日志文件名格式
# 1.2.3 调整日志函数
# 1.2.2 日志增加时间戳
# 1.2.1 增加日志
# 1.2.0 调整定时任务5秒执行一次
# 1.1.9 调整卖出策略
# 1.1.8 增加单只股票买入成功状态
# 1.1.7 调整卖出开始时间为9点50分
# 1.1.6 控制买入交易费率为千分之五
# 1.1.5 新增修订购买力不超过持仓比例
# 1.1.4 调整空余仓位到50%
# 1.1.3 调整止盈比例为1%
# 1.1.2 调整止盈比例为2%, 仓位可用40%
# 1.1.1 新增卖出定时任务
# 1.1.0 收敛交易时间范围
# 1.0.9 新增float四舍五入函数
# 1.0.8 新增部分系统的回调函数
# 1.0.7 调整初始化任务
# 1.0.6 校验策略订单(文件)创建日期和交易日是否匹配
# 1.0.5 收敛日期格式
# 1.0.4 自动适配证券代码
# 1.0.3 自动适配单一股票最大可用资金
# 1.0.2 增加资金账户信息, 最大买入持仓率
# 1.0.1 调整部分文件名
# 1.0.0 新建交易脚本
import json
import math
import os
import time
from datetime import datetime
from decimal import Decimal, ROUND_HALF_UP

import numpy as np
import pandas as pd

kFormatFileDate = '%Y%m%d'
kFormatOnlyDate = '%Y-%m-%d'
kFormatTimestamp = '%Y-%m-%d %H:%M:%S'
errBadSymbol = RuntimeError("无法识别的证券代码")

# 定制任务执行频次
timer_period = 5
# 策略名称 - 买入
strategy_name_buy = 'T89K_BUY'
# 策略名称 - 卖出
strategy_name_sell = 'T89K_SELL'
# 操作类型 - 买入
opetation_type_buy = 23
# 操作类型 - 卖出
opetation_type_sell = 24
# 设定是否立即触发下单, 参数设置为2时，不判断bar状态，只要策略模型中调用到就触发下单交易，历史bar上也能触发下单，请谨慎使用
quick_trade = 2
# 下单选价类型: 0-卖5价, 11-(指定价)模型价(只对单股情况支持,对组合交易不支持), 14-对手价(对方盘口一档价)
price_type_buy = 0
price_type_sell = 14

# 订单类型 - 买入 - 1101, 单股、单账号、普通、股/手方式下单
order_type_buy = 1101
# 订单类型 - 卖出 - 1101, 单股、单账号、普通、股/手方式下单
order_type_sell = 1101

# 根路径
root_path = 'Y:/qmt'
# 买入持仓率, 资金控制阀值
position_ratio = 0.5000
# 相对开盘价溢价多少买入
buy_premium_rate = 0.0200
# 买入交易费率
buy_trade_rete = 0.0250
# 卖出 - 止损 - 比例
sell_stop_loss_ratio = 0.0000
# 卖出 - 止赢 - 比例
sell_stop_profit_ratio = 0.0000
# 任务函数名 - 买入
taskFunctionBuy = 'quant1x_trade_t89k_buy'
# 任务函数名 - 卖出
taskFunctionSell = 'quant1x_trade_t89k_sell'
# 竞价开始时间 - 买入
bidding_begin = '09:25:00'
# 竞价结束时间 - 买入
bidding_end = '14:59:30'
# 竞价开始时间 - 卖出
ask_begin = '09:50:00'
# 竞价结束时间 - 卖出
ask_end = '14:59:30'
# 订单时间差
order_time_diff = 10


def init(ctx):
    """
    初始化函数
    :param ctx:
    :return:
    """
    load_config(ctx)
    # 集合竞价定时任务下单-买入
    ctx.run_time(taskFunctionBuy, f'{timer_period}nSecond', time.strftime(kFormatOnlyDate) + f' {bidding_begin}')
    # 集合竞价定时任务下单-卖出
    ctx.run_time(taskFunctionSell, f'{timer_period}nSecond', time.strftime(kFormatOnlyDate) + f' {ask_begin}')


def after_init(ctx):
    """
    系统会在init函数执行完后和执行handlebar之前调用after_init
    :param ctx:
    :return:
    """
    account_init(ctx)


def handlebar(ctx):
    """
    K线触发函数
    :param ctx:
    :return:
    """
    pass


def stop(ctx):
    """
    终止函数
    :param ctx:
    :return:
    """
    print(f'{taskFunctionBuy} stopped at ', time.strftime(kFormatTimestamp))


def order_callback(ctx, orderInfo):
    """
    委托状态变化主推
    :param ctx:
    :param orderInfo:
    :return:
    """
    print('订单回报', orderInfo.m_strRemark, orderInfo.m_strOrderSysID)
    # QMT证券代码
    # qmt_code = orderInfo.m_strInstrumentID + '.' + orderInfo.m_strExchangeID
    # Quant1X 证券代码
    # code = (orderInfo.m_strExchangeID + orderInfo.m_strInstrumentID).lower()
    log(ctx, repr(orderInfo))


def orderError_callback(ctx, orderArgs, errMsg):
    """
    异常下单主推
    :param ctx:
    :param orderArgs:
    :param errMsg:
    :return:
    """
    print(orderArgs, errMsg)
    log(ctx, repr(orderArgs))
    log(ctx, repr(errMsg))


def deal_callback(ctx, dealInfo):
    """
    成交状态变化主推
    :param ctx:
    :param dealInfo:
    :return:
    """
    print('成交回报', dealInfo.m_strRemark, dealInfo.m_strOrderSysID)
    log(ctx, dealInfo)


def quant1x_trade_t89k_buy(ctx):
    """
    定时任务 - 买入
    :param ctx:
    :return:
    """
    print(taskFunctionBuy + '...')
    log(ctx, '开始扫描订单')
    today = time.strftime(kFormatOnlyDate)
    if not os.path.exists(ctx.t89k_order_file):
        print(f'{today}订单未生成, waiting...')
        return
    mtime = os.path.getmtime(ctx.t89k_order_file)
    modTime = datetime.fromtimestamp(mtime)
    mod_date = modTime.strftime(kFormatOnlyDate)
    df = pd.read_csv(ctx.t89k_order_file)
    if len(df) > 0:
        trade_date = df['date'][0]
        if trade_date != mod_date:
            print('交易日和订单文件日期不匹配')
            return
    else:
        print('订单为空')
        return

    # if os.path.isfile(ctx.t89k_flag_done):
    #     # 如果done文件存在, 则直接退出
    #     print(" ### Trade finished already, trade done flag file: ", ctx.t89k_flag_done)
    #     return
    if not os.path.isfile(ctx.t89k_flag_ready):
        # 如果策略文件输出完成的标志性文件不存在, 则退出
        print(" ### Wait for strategy out ready file: ", ctx.t89k_flag_ready)
        return
    if os.path.isfile(ctx.t89k_order_file):
        # 如果策略文件已存在, 读取
        df = pd.read_csv(ctx.t89k_order_file)
        stock_total = len(df)
        if stock_total == 0:
            # 股票数量等于0, 直接返回
            return
        # 获取当前时间戳
        tmLocal = datetime.now()
        order_start = datetime.strptime(f'{today} {bidding_begin}', kFormatTimestamp)
        order_end = datetime.strptime(f'{today} {bidding_end}', kFormatTimestamp)

        # 遍历订单, 数据文件只需要code和open两个字段
        for idx, stock in df.iterrows():
            # 数据日期
            stock_date = stock['date']
            # 证券代码
            code = stock['code']
            # 检查买入成功标识
            if checkBuyOrderDoneStatus(ctx, code):
                continue
            # 订单的时间戳
            tmOrder = datetime.strptime(stock['update_time'], kFormatTimestamp)
            # 更新日期
            update_date = tmOrder.strftime(kFormatOnlyDate)
            if stock_date != update_date:
                print('交易日期和策略输出日期不匹配')
                stock_total = stock_total - 1
                continue
            # 计算单一可用资金
            single_funds_available = fix_single_available(ctx, stock_total)
            # 扣除可能发生的交易费率
            single_funds_available = single_funds_available * (1 - buy_trade_rete)
            # if order_start <= tmLocal <= order_end or (tmLocal - tmOrder).seconds < order_time_diff:
            if order_start <= tmLocal <= order_end or (tmLocal - tmOrder).seconds < order_time_diff:
                # 09:25 到 09:30 返回的时间信息是 [ 09:30 ], 或者相差小于 10s 检查通过
                print(stock)
                security_code = fix_security_code(code)
                # 计算溢价
                buy_price = stock['open'] * (1 + buy_premium_rate)
                buy_price = price_round(buy_price)
                buy_num = math.floor(single_funds_available / (buy_price * 100)) * 100
                if buy_num == 0:
                    print('单一股价过高, 分仓购买力不足1手')
                    stock_total = stock_total - 1
                    continue
                log(ctx, f'{ctx.account_id}: 证券代码={security_code}, 委托价格={buy_price}, 委托数量={buy_num}')
                passorder(opetation_type_buy, order_type_buy, ctx.account_id, security_code, price_type_buy, buy_price,
                          buy_num, strategy_name_buy,
                          quick_trade, ctx)
                # 设置执行下单完成状态
                pushBuyOrderDoneStatus(ctx, code)
            else:
                print('----------------< 交易时间范围之外的订单 >----------------')
                print("stock %s [ %s ] price time expired [ %s, space: %d ]" %
                      (stock['code'],
                       stock['name'],
                       stock['update_time'],
                       (tmLocal - tmOrder).seconds))

        # 设置已完成标志文件
        push_local_message(ctx.t89k_flag_done)
        # 设置板块成分股, 例如自选股列表
        reset_sector_stock_list('T89KQT', df['code'].values.tolist())
    else:
        pass


def quant1x_trade_t89k_sell(ctx):
    """
    定时任务 - 卖出
    :param ctx:
    :return:
    """
    print(taskFunctionSell + '...')
    today = time.strftime(kFormatOnlyDate)
    # 获取当前时间戳
    tmLocal = datetime.now()
    order_start = datetime.strptime(f'{today} {ask_begin}', kFormatTimestamp)
    order_end = datetime.strptime(f'{today} {ask_end}', kFormatTimestamp)
    if tmLocal <= order_start or tmLocal >= order_end:
        print('非交易时段, 终止卖出操作')
        return
    # 获取持仓
    positions = get_trade_detail_data(ctx.account_id, 'stock', 'position')
    for dt in positions:
        security_code = dt.m_strInstrumentID + '.' + dt.m_strExchangeID
        op_flag = 'Unknown'
        if dt.m_nCanUseVolume >= 100:
            ask_volume = math.floor(dt.m_nCanUseVolume / 100) * 100
            # 14 为对手价
            if dt.m_dProfitRate < -sell_stop_loss_ratio or dt.m_dProfitRate > sell_stop_profit_ratio:
                passorder(opetation_type_sell, order_type_sell, ctx.account_id, security_code, price_type_sell, -1,
                          ask_volume, strategy_name_sell, quick_trade, ctx)
                op_flag = 'ASKING'
            else:
                op_flag = 'WAITING'
        else:
            op_flag = 'SKIP'
        # 控制台输出持仓记录
        print("%s stock %s [ %s ] holding: %d can_ask: %d cost: %.2f chg: %.2f" % (op_flag, security_code,
                                                                                   dt.m_strInstrumentName,
                                                                                   dt.m_nVolume,
                                                                                   dt.m_nCanUseVolume,
                                                                                   dt.m_dPositionCost,
                                                                                   dt.m_dProfitRate))


def log(ctx, obj):
    """
    记录日志
    :param message:
    :return:
    """
    today = time.strftime(kFormatFileDate)
    log_filename = os.path.join(ctx.order_path, f'{today}-{ctx.account_id}.log')
    if isinstance(obj, str):
        text = obj
    else:
        text = json.dumps(obj)
    tm = datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
    message = f'[{tm}] {text}\n'
    # message = text.encode('unicode_escape').decode('utf-8')
    # message = text.encode('unicode_escape').decode('gbk')
    with open(log_filename, "a", encoding="utf-8") as file:
        file.write(message)


def getOrderFlag(ctx, code: str, type: int) -> str:
    """
    获取订单标识
    :param ctx:
    :param code:
    :param type: 1-b,2-s
    :return:
    """
    today = time.strftime(kFormatFileDate)
    order_type = "b" if type == 1 else "s"
    stock_order_flag = os.path.join(ctx.order_path, f'{today}-{ctx.account_id}-{code}-{order_type}.done')
    return stock_order_flag


def checkBuyOrderDoneStatus(ctx, code: str) -> bool:
    """
    检查买入订单执行完成状态
    :return:
    """
    flag = getOrderFlag(ctx, code, 1)
    return os.path.exists(flag)


def pushBuyOrderDoneStatus(ctx, code: str):
    """
    推送买入订单完成状态
    :param ctx:
    :param code:
    :return:
    """
    flag = getOrderFlag(ctx, code, 1)
    push_local_message(flag)


def resetBuyOrder(ctx, code: str):
    """
    重置买入点单完成状态
    :param ctx:
    :param code:
    :return:
    """
    flag = getOrderFlag(ctx, code, 1)
    if os.path.exists(flag):
        os.remove(flag)


def load_config(ctx):
    """
    加载配置文件
    :param ctx:
    :return:
    """
    # 加载配置文件 begin
    ctx.order_path = root_path
    account_file = os.path.join(ctx.order_path, 'account.txt')
    if not os.path.isfile(account_file):
        print('config your account to file: %s, please.' % (account_file))
        return
    # 设置账号
    ctx.account_id = ""
    with open(account_file) as fd:
        ctx.account_id = fd.readline().strip()
    ctx.set_account(ctx.account_id)
    print(" ### load account: ", ctx.account_id)
    # 加载配置文件 end

    today = time.strftime(kFormatFileDate)
    ctx.t89k_flag_ready = os.path.join(ctx.order_path, f'{today}.ready')
    ctx.t89k_flag_done = os.path.join(ctx.order_path, f'{today}-{ctx.account_id}.done')
    ctx.t89k_order_file = os.path.join(ctx.order_path, today + '-t10.csv')


def account_init(ctx):
    """
    初始化账户
    :param ctx:
    :return:
    """
    # 获取账户概要
    # print('account_init - 1')
    accounts = get_trade_detail_data(ctx.account_id, 'stock', 'account')
    # print('account_init - 2')
    for dt in accounts:
        print(f'总资产: {dt.m_dBalance:.2f}, 净资产: {dt.m_dAssureAsset:.2f}, 总市值: {dt.m_dInstrumentValue:.2f}',
              f'总负债: {dt.m_dTotalDebit:.2f}, 可用金额: {dt.m_dAvailable:.2f}, 盈亏: {dt.m_dPositionProfit:.2f}')

    # 设置持仓率
    ctx.position_ratio = position_ratio
    ctx.m_dBalance = 0.00  # 总资产
    ctx.m_dAvailable = 0.00  # 可用金额
    ctx.quant_dAvailable = 0.00  # 量化可用金额
    if len(accounts) > 0:
        ctx.account = accounts[0]
        ctx.m_dBalance = ctx.account.m_dBalance
        ctx.m_dAvailable = ctx.account.m_dAvailable
        # 计算可用资金占比
        avaiableRatio = ctx.m_dAvailable / ctx.m_dBalance
        # 设置持仓率
        if not isNaN(avaiableRatio) and avaiableRatio >= position_ratio:
            ctx.quant_dAvailable = ctx.m_dBalance * position_ratio


def fix_single_available(ctx, total: int) -> float:
    """
    调整单一可用资金
    :param ctx:
    :param total:
    :return:
    """
    accounts = get_trade_detail_data(ctx.account_id, 'stock', 'account')
    # print('account_init - 2')
    if len(accounts) > 0:
        ctx.account = accounts[0]
        ctx.m_dBalance = ctx.account.m_dBalance
        ctx.m_dAvailable = ctx.account.m_dAvailable
        ctx.m_dInstrumentValue = ctx.account.m_dInstrumentValue
    # 修订可以量化用金额
    ctx.quant_dAvailable = ctx.m_dAvailable
    if ctx.quant_dAvailable / ctx.m_dBalance > position_ratio:
        ctx.quant_dAvailable = ctx.m_dBalance * position_ratio
    if ctx.quant_dAvailable > ctx.m_dAvailable:
        ctx.quant_dAvailable = ctx.m_dAvailable
    if total > 0:
        ctx.single_funds_available = ctx.quant_dAvailable / total
    else:
        ctx.single_funds_available = 0.00
    return ctx.single_funds_available


def push_local_message(filename: str):
    """
    推送消息
    :param filename:
    :return:
    """
    with open(filename, 'w') as done_file:
        pass


def isNaN(n) -> bool:
    """
    判断是否nan或inf
    :param n:
    :return:
    """
    return np.isnan(n) or np.isinf(n)


def price_round(num: float, digits: int = 2) -> float:
    """
    价格四舍五入
    :param num:
    :param digits: 小数点后几位数字
    :return:
    """
    if isinstance(num, float):
        num = str(num)
    x = Decimal(num).quantize((Decimal('0.' + '0' * digits)), rounding=ROUND_HALF_UP)
    return float(x)


def fix_security_code(symbol: str) -> str:
    """
    调整证券代码
    :param symbol:
    :return:
    """
    security_code = ''
    if len(symbol) == 6:
        flag = get_security_type(symbol)
        security_code = f'{symbol}.{flag}'
    elif len(symbol) == 8 and symbol[:2] in ["sh", "sz", "SH", "SZ"]:
        security_code = symbol[2:] + '.' + symbol[:2].upper()
    else:
        raise errBadSymbol
    return security_code


def get_security_type(symbol: str) -> str:
    """
    获取股票市场标识
    :param symbol:  代码
    :return:
    """
    if len(symbol) != 6:
        raise errBadSymbol
    code_head = symbol[:2]
    if code_head in ["00", "30"]:
        return "SZ"
    if code_head in ["60", "68"]:  # 688XXX科创板
        return "SH"
    if code_head in ["510"]:
        return "SH"
    raise errBadSymbol
