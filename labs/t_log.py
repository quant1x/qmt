import json
from datetime import datetime

text = "你好"
d1 = text.encode("utf-8").decode('utf-8')
print(d1)
d2 = d1.encode('utf-8').decode('gbk')
print(d2)
message = d2.encode('gbk').decode('utf-8')
print(message)

s = "123"
s1 = json.dumps(s)
print(s1)
s2 = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
print(s2)
