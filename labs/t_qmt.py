import os
import time
from datetime import datetime
from decimal import Decimal, ROUND_HALF_UP

import pandas as pd
import yaml


def right_round(num: float, keep_n: int = 2) -> float:
    if isinstance(num, float):
        num = str(num)
    x = Decimal(num).quantize((Decimal('0.' + '0' * keep_n)), rounding=ROUND_HALF_UP)
    return float(x)


print(right_round(1.245))

kFormatOnlyDate = '%Y-%m-%d'

today = time.strftime("%Y%m%d")
today = '20230823'
runtime_path = '/Users/wangfeng/.quant1x/qmt'
t89k_order_file = os.path.join(runtime_path, today + '-t10.csv')
mtime = os.path.getmtime(t89k_order_file)
modTime = datetime.fromtimestamp(mtime)
mod_date = modTime.strftime(kFormatOnlyDate)
df = pd.read_csv(t89k_order_file)
if len(df) > 0:
    print(df['code'].values.tolist())
    var = df['date'][0]
    print(var, mod_date)
with open(runtime_path + '/qmt.yaml', 'r', encoding='utf-8') as f:
    result = yaml.load(f.read(), Loader=yaml.FullLoader)
    print(result['accountId'])
